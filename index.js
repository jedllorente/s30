const express = require('express');
const mongoose = require('mongoose');

const port = 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

mongoose.connect('mongodb+srv://admin:admin@cluster0.3zkqs.mongodb.net/session30?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once('open', () => console.log('Connected to the cloud database.'));

/* create user schema */
const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

/* user model */
const User = mongoose.model('User', userSchema);

/* post route */
app.post('/signup', (req, res) => {
    User.findOne({ username: req.body.username }, (err, result) => {
        if (result != null && result.username == req.body.username) {
            return res.send('User already exists.')
        } else {
            let newTask = new User({ username: req.body.username });
            newTask.save((saveErr, savedUser) => {
                if (saveErr) {
                    return console.error(saveErr);

                } else {
                    return res.status(201).send('New User Created.');
                }
            })
        }
    })
})


/* retrieve all users */
app.get('/signup', (req, res) => {
    User.find({}, (err, result) => {
        if (err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data: result
            })
        }
    });

})

/* Port listener */
app.listen(port, () => console.log(`Server is running at localhost ${port}`));